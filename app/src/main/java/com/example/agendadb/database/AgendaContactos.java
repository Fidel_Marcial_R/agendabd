package com.example.agendadb.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

public class AgendaContactos {
    private Context context;
    private AgendaDbHelper agendaDbHelper;
    private SQLiteDatabase db;

    private String [] columnToRead = new String[]{
      DefinirTabla.contacto._ID,
            DefinirTabla.contacto.NOMBRE,
            DefinirTabla.contacto.TELEFONO1,
            DefinirTabla.contacto.TELEFONO2,
            DefinirTabla.contacto.DOMICILIO,
            DefinirTabla.contacto.NOTAS,
            DefinirTabla.contacto.FAVORITO
    };

    public AgendaContactos(Context context){
        this.context = context;
        agendaDbHelper =  new AgendaDbHelper(this.context);
    }
    public void openDataBase(){
        db = agendaDbHelper.getWritableDatabase();
    }
    public long insertarContactos(contacto c)
    {
        ContentValues values =new ContentValues();
        values.put(DefinirTabla.contacto.NOMBRE,c.getNombre());
        values.put(DefinirTabla.contacto.TELEFONO1,c.getTelefono1());
        values.put(DefinirTabla.contacto.TELEFONO2,c.getTelefono2());
        values.put(DefinirTabla.contacto.DOMICILIO,c.getDomicilio());
        values.put(DefinirTabla.contacto.NOTAS,c.getNotas());
        values.put(DefinirTabla.contacto.FAVORITO,c.getFavorito());
        return db.insert(DefinirTabla.contacto.TABLE_NAME, null, values);
    }

    public long UpdateContacto(contacto c, long id) {
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.contacto.NOMBRE, c.getNombre());
        values.put(DefinirTabla.contacto.TELEFONO1, c.getTelefono1());
        values.put(DefinirTabla.contacto.TELEFONO2, c.getTelefono2());
        values.put(DefinirTabla.contacto.DOMICILIO, c.getDomicilio());
        values.put(DefinirTabla.contacto.NOTAS, c.getNotas());
        values.put(DefinirTabla.contacto.FAVORITO, c.getFavorito());
        String criterio = DefinirTabla.contacto._ID + "=" + id;
        return db.update(DefinirTabla.contacto.TABLE_NAME, values, criterio, null);

    }

    public int deleteContactos(long id){
        String criterio = DefinirTabla.contacto._ID + "=" + id;
        return db.delete(DefinirTabla.contacto.TABLE_NAME, criterio,null);
    }

    public contacto readContacto(Cursor cursor){
        contacto c = new contacto();
        c.setID(cursor.getInt(0));
        c.setNombre(cursor.getString(1));
        c.setTelefono1(cursor.getString(2));
        c.setTelefono2(cursor.getString(3));
        c.setDomicilio(cursor.getString(4));
        c.setNotas(cursor.getString(5));
       /* c.setFavorito(cursor.getInt(6));*/
        return c;
    }
    public contacto getContacto(long id){
        contacto con = null;
        SQLiteDatabase db = agendaDbHelper.getReadableDatabase();
        Cursor c = db.query(DefinirTabla.contacto.TABLE_NAME, columnToRead, DefinirTabla.contacto._ID + " = ? ", new String[]{String.valueOf(id)}, null, null, null);


    //////Agregar la condicion del if

        if(c.moveToFirst())
        con = readContacto(c);
        c.close();
        return con;

    }

    public ArrayList<contacto> allContactos(){
        ArrayList<contacto> contactos = new ArrayList<contacto>();
        Cursor cursor = db.query(DefinirTabla.contacto.TABLE_NAME, null, null, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()){
            contacto c = readContacto(cursor);
            contactos.add(c);
            cursor.moveToNext();

        }
        cursor.close();
        return contactos;

    }
    public void cerrar(){
        agendaDbHelper.close();
    }


}
