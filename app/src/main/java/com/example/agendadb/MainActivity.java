package com.example.agendadb;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.agendadb.database.AgendaContactos;
import com.example.agendadb.database.AgendaDbHelper;
import com.example.agendadb.database.contacto;

public class MainActivity extends AppCompatActivity {

    private Button btnRespuesta, btnBuscar;
    private TextView lblRespuesta, lblnombre;
    private EditText txtId;
    /*Nuevas variables*/
    private EditText edtNombre;
    private EditText edtTelefono;
    private EditText edtTelefono2;
    private EditText edtDireccion;
    private EditText edtNotas;
    private CheckBox cheFavorito;


    private contacto saveContact;
    private int id;
    private AgendaContactos db;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        lblnombre = (TextView) findViewById(R.id.lblNombre);
        /*Enlace con las formas*/

        edtNombre = (EditText) findViewById(R.id.txtNombre);
        edtTelefono = (EditText) findViewById(R.id.txtTel1);
        edtTelefono2 = (EditText) findViewById(R.id.txtTel2);
        edtDireccion = (EditText) findViewById(R.id.txtDomicilio);
        edtNotas = (EditText) findViewById(R.id.txtNota);
        cheFavorito = (CheckBox) findViewById(R.id.chkFavorito);

        db = new AgendaContactos(MainActivity.this);


        Button btnGuardar = (Button) findViewById(R.id.btnGuardar);
        Button btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        Button btnListar = (Button) findViewById(R.id.btnListar);
        Button btnSalir = (Button) findViewById(R.id.btnSalir);




        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edtNombre.getText().toString().equals("") || edtDireccion.getText().toString().equals("")
                        || edtTelefono.getText().toString().equals("")){
                    Toast.makeText(MainActivity.this, R.string.msgerror, Toast.LENGTH_SHORT).show();
                }else{
                    contacto nContacto = new contacto();
                    nContacto.setNombre(edtNombre.getText().toString());
                    nContacto.setTelefono1(edtTelefono.getText().toString());
                    nContacto.setTelefono2(edtTelefono2.getText().toString());
                    nContacto.setDomicilio(edtDireccion.getText().toString());
                    nContacto.setNotas(edtNotas.getText().toString());
                    if (cheFavorito.isChecked()){
                        nContacto.setFavorito(true);
                    }else{
                        nContacto.setFavorito(false);
                    }


                        db.openDataBase();
                    if(saveContact == null){
                        long idx = db.insertarContactos(nContacto);
                        Toast.makeText(MainActivity.this, "Se agrego el contacto con el id" + id,Toast.LENGTH_SHORT).show();

                    }else{
                        db.UpdateContacto(nContacto, id);
                        Toast.makeText(MainActivity.this, "Se actualizo el contacto con el id" + id,Toast.LENGTH_SHORT).show();
                    }
                        db.cerrar();

                }
            }
        });
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Limpiar();
            }
        });
        btnListar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i =  new Intent(MainActivity.this, ListActivity.class);
               /* Bundle nObject =  new Bundle();*/
                /*nObject.putSerializable("contactos", contacto);*/
               /* i.putExtras(nObject);*/
                startActivityForResult(i, 0);
            }
        });
        btnBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                contacto contacto;
                String validar = txtId.getText().toString();
                if(validar.equals("")){
                    Toast.makeText(MainActivity.this,"Ingrese valores ",Toast.LENGTH_SHORT).show();
                }else{
                    long id = Long.parseLong(txtId.getText().toString());
                db.openDataBase();
                contacto =  db.getContacto(id);
                if (contacto == null){
                    lblnombre.setText("No exixte un contacto con esa id");

                }else {
                    lblnombre.setText(contacto.getNombre());
                }db.cerrar();
            }}
        });

    }
    public void Limpiar()
    {

        edtNombre.setText("");
        edtTelefono.setText("");
        edtTelefono2.setText("");
        edtDireccion.setText("");
        edtNotas.setText("");
        cheFavorito.setChecked(false);

    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if (Activity.RESULT_OK == resultCode){
            contacto con = (contacto) data.getSerializableExtra("contacto");
            saveContact = con;
            long txtId = con.getID();
            edtNombre.setText(con.getNombre());
            edtDireccion.setText(con.getDomicilio());
            edtTelefono.setText(con.getTelefono1());
            edtTelefono2.setText(con.getTelefono2());
            edtNotas.setText(con.getNotas());
            if (con.isFavorito() == true){
                cheFavorito.setChecked(true);
            }

        }else{
            Limpiar();
        }
    }
}
